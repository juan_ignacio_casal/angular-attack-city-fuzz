import { CityfuzzPage } from './app.po';

describe('cityfuzz App', function() {
  let page: CityfuzzPage;

  beforeEach(() => {
    page = new CityfuzzPage();
  })

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('cityfuzz works!');
  });
});
