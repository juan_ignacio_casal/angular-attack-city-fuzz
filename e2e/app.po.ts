export class CityfuzzPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('cityfuzz-app h1')).getText();
  }
}
