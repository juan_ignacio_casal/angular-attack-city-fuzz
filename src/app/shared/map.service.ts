import { Injectable } from '@angular/core';

@Injectable()
export class MapService {
  editing: boolean = false;
  currentCoords: any;
  constructor() {
  }

  getCurrentLocation() {
    return new Promise<any>((resolve, reject) => {
      window.navigator.geolocation.getCurrentPosition((geoPosition:any) => {
        resolve(geoPosition.coords);
      });
    });
  }
}
