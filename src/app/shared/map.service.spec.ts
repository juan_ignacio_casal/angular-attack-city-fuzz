import {
  beforeEachProviders,
  it,
  xit,
  describe,
  expect,
  inject
} from '@angular/core/testing';
import { MapService } from './map.service';

describe('Map Service', () => {
  beforeEachProviders(() => [MapService]);

  it('should be defined',
      inject([MapService], (service: MapService) => {
    expect(service).toBeTruthy();
  }));

  xit('should use html5 geolocation if available', inject([MapService], (service: MapService) => {
    throw new Error('not implemented test');
  }));

  xit('should use ip-based location if html5 navigator not available', inject([MapService], (service: MapService) => {
    throw new Error('not implemented test');
  }));

  xit('should use a default value if there is not connection and html5 geolocation is not available', inject([MapService], (service: MapService) => {
    throw new Error('not implemented test');
  }));
});
