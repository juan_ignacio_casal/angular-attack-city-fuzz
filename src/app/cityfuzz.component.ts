import { Component } from '@angular/core';
import { Routes, Router, ROUTER_DIRECTIVES } from '@angular/router';

import { MapService } from './shared';
import { EventService, EventListComponent, EventCreateComponent } from './events';
import { CategoryService } from './categories';

@Component({
  moduleId: module.id,
  selector: 'cityfuzz-app',
  templateUrl: 'cityfuzz.component.html',
  styleUrls: ['cityfuzz.component.css'],
  directives: [ ROUTER_DIRECTIVES ],
  providers: [ MapService, EventService, CategoryService ]
})
@Routes([
  { path: '/', component: EventListComponent },
  { path: '/event/new', component: EventCreateComponent }
])
export class CityfuzzAppComponent {
  constructor(private router: Router) {}

  title = 'City Fuzz';
}
