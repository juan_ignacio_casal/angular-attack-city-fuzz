import { AfterViewInit, Directive, ElementRef } from '@angular/core';

declare var componentHandler;

@Directive({
  selector: '[mdl]'
})
export class MDL implements AfterViewInit {
  constructor(private el: ElementRef) {}

  ngAfterViewInit() {
    window['componentHandler'].upgradeElement(this.el.nativeElement);
  }
}
