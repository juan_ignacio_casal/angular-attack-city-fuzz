import { Component, OnInit } from '@angular/core';
import { NgClass } from '@angular/common';
import { Router } from '@angular/router';
import { MapService } from '../shared/map.service'
import { MDL } from '../mdl.directive';

@Component({
  moduleId: module.id,
  selector: 'sidemenu',
  templateUrl: 'sidemenu.component.html',
  styleUrls: ['sidemenu.component.css'],
  directives: [ MDL, NgClass ]
})

export class SidemenuComponent implements OnInit {

  isCollapsed:boolean = false;
  constructor(private mapService:MapService, private router:Router) {}

  ngOnInit() {
  }

  toggleSidemenu() {
    this.isCollapsed = !this.isCollapsed;
  }

  createEvent() {
    //this.mapService.editing = true;
    this.router.navigate(['/event/new']);
  }

  cancelEvent() {
    this.mapService.editing = false;
  }
}
