import {
  beforeEach,
  beforeEachProviders,
  describe,
  expect,
  it,
  xit,
  inject,
} from '@angular/core/testing';
import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { SidemenuComponent } from './sidemenu.component';

describe('Component: Sidemenu', () => {
  let builder: TestComponentBuilder;

  beforeEachProviders(() => [SidemenuComponent]);
  beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
    builder = tcb;
  }));

  it('should inject the component', inject([SidemenuComponent],
      (component: SidemenuComponent) => {
    expect(component).toBeTruthy();
  }));

  xit('should create the component', inject([], () => {
    return builder.createAsync(SidemenuComponentTestController)
      .then((fixture: ComponentFixture<any>) => {
        let query = fixture.debugElement.query(By.directive(SidemenuComponent));
        expect(query).toBeTruthy();
        expect(query.componentInstance).toBeTruthy();
      });
  }));
});

@Component({
  selector: 'test',
  template: `
    <app-sidemenu></app-sidemenu>
  `,
  directives: [SidemenuComponent]
})
class SidemenuComponentTestController {
}

