import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { CityfuzzAppComponent } from '../app/cityfuzz.component';
import { MapService} from '../app/shared/map.service';

beforeEachProviders(() => [CityfuzzAppComponent, MapService]);

describe('App: Cityfuzz', () => {
  it('should create the app',
      inject([CityfuzzAppComponent], (app: CityfuzzAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'City Fuzz\'',
      inject([CityfuzzAppComponent], (app: CityfuzzAppComponent) => {
    expect(app.title).toEqual('City Fuzz');
  }));
});
