import { Injectable } from '@angular/core';
import { Category } from './category.model';

@Injectable()
export class CategoryService {

  constructor() {}

  getCategories() {
    return Promise.resolve(CATEGORIES);
  }

}

const CATEGORIES:Category[] = [
  { id: '1', title: 'Nightlife'},
  { id: '2', title: 'Promotion'},
  { id: '3', title: 'Live Bands'},
  { id: '4', title: 'Party'},
];
