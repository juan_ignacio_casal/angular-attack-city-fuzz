import { Component, ElementRef, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { ANGULAR2_GOOGLE_MAPS_DIRECTIVES } from 'angular2-google-maps/core';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { MapService } from '../shared';
import { Event, EventService, EventComponent } from '../events';
import { MDL } from '../mdl.directive';

@Component({
  moduleId: module.id,
  selector: 'cf-map',
  templateUrl: 'map.component.html',
  styleUrls: ['map.component.css'],
  directives: [ANGULAR2_GOOGLE_MAPS_DIRECTIVES, MDL, EventComponent]
})

export class MapComponent implements OnInit {
  @Input() events: FirebaseListObservable<any[]>;
  @Output() markerSelected = new EventEmitter();
  @Output() mapClicked = new EventEmitter();

  @Input() createMode: boolean;

  private lat: number;
  private lng: number;
  private loadingLocation: boolean = true;
  private fakeEvent: Event = new Event();

  constructor(
    private mapService: MapService,
    private el: ElementRef
  ) {}

  ngOnInit() {


    if(this.createMode && this.mapService.currentCoords) {
      this.lat = this.mapService.currentCoords.lat;
      this.lng = this.mapService.currentCoords.lng;
      this.loadingLocation = false;
    }
    else {
      this.mapService.getCurrentLocation().then(coords => {
        this.lat = coords.latitude;
        this.lng = coords.longitude;
        this.mapService.currentCoords = {lat: this.lat, lng: this.lng};
        this.loadingLocation = false;
      });
    }
  }

  onMapClicked(coords) {
    if(this.createMode) {
      this.mapService.currentCoords = coords;
      this.lat = coords.lat;
      this.lng = coords.lng;

      this.fakeEvent.lat = this.lat;
      this.fakeEvent.lng = this.lng;

      this.mapClicked.emit({
        lat: coords.lat,
        lng: coords.lng
      });
    }
  }

  markerClick(e) {
    if(!this.mapService.editing) {
      this.markerSelected.emit({
        event: e
      });
    }
  }
}
