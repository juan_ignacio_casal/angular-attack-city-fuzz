export class Event {
  id: string;
  title: string;
  description: string;
  eta: string;
  createdat: string;
  lat: number;
  lng: number;
  categories: string[];
}
