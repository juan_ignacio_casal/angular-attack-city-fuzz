import {
  beforeEachProviders,
  it,
  xit,
  describe,
  expect,
  inject
} from '@angular/core/testing';
import { EventService } from './event.service';

describe('Event Service', () => {
  beforeEachProviders(() => [EventService]);

  xit('should return list of events',
      inject([EventService], (service: EventService) => {

    let events = service.getEvents();

    // events.then(result => {
    //   expect(result).to.have.length(2);
    // });
  }));
});
