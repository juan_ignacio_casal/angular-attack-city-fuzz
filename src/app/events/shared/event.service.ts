import { Injectable } from '@angular/core';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { Event } from './event.model';

@Injectable()
export class EventService {
  events: FirebaseListObservable<any[]>;
  constructor(private af: AngularFire) {
    this.events = this.af.database.list('/events');
  }

  getEvents() {
    return this.events;
  }

  addEvent(event:Event) {
    event.createdat = Date.now.toString();
    this.events.push(event);
  }
}

const EVENTS: Event[] = [
  {
    id: '1',
    title: "Charlie's party",
    description: 'Bring your own drink',
    eta: '05:00 hs',
    createdat: '14-05-2016 21:00:00',
    lat: -34.537159, lng: -56.189844,
    categories: ['Live Bands', 'Party']
  },
  {
    id: '2',
    title: 'Salsa dancing',
    description: 'From sundown to sunrise',
    eta: '05:00 hs',
    createdat: '14-05-2016 21:00:00',
    lat: -33.5230788089042,
    lng: -57.864990234375,
    categories: []
  }
];
