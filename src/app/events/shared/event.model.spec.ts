import {
  describe,
  ddescribe,
  expect,
  iit,
  it
} from '@angular/core/testing';
import {Event} from './event.model';

describe('Event', () => {
  it('should create an instance', () => {
    expect(new Event()).toBeTruthy();
  });

  it('should have a title and description', () => {
    let event: Event = {
      id: 'test',
      title: 'Food Party',
      description: 'Best potluck in town!',
      eta: '',
      createdat: '',
      lat: 10,
      lng: 20,
      categories: []
    };

    expect(event.title).toEqual('Food Party');
    expect(event.description).toEqual('Best potluck in town!');
    expect(event.lat).toEqual(10);
    expect(event.lng).toEqual(20);
  });
});
