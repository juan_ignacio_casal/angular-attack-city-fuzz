import { Component, OnInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FORM_DIRECTIVES } from '@angular/common';
import { MDL } from '../../mdl.directive';
import { Event, EventService } from '../shared';
import { MapService } from '../../shared';
import { MapComponent } from '../../map';
import { Category, CategoryService } from '../../categories/shared';

@Component({
  moduleId: module.id,
  selector: 'app-event-create',
  templateUrl: 'event-create.component.html',
  styleUrls: ['event-create.component.css'],
  directives: [MapComponent, FORM_DIRECTIVES, MDL]
})
export class EventCreateComponent implements OnInit {
  event:Event;
  categories:Array<Category>;

  create:boolean = true;
  wizardStep:string = 'create';
  cancelBtnText:string = 'Cancel';
  nextBtnText:string = 'Next';
  showNextBtn:boolean = false;
  showSubmitBtn:boolean = false;
  showSuccessBtn:boolean = false;
  showCancelBtn:boolean = true;

  step1:boolean = true;
  step2:boolean = false;
  step3:boolean = false;

  constructor(
    private el: ElementRef,
    private mapService: MapService,
    private eventService: EventService,
    private categoryService: CategoryService,
    private router:Router
  ) {}

  ngOnInit() {
    this.categoryService.getCategories().then(categories => {
      this.categories = categories;
    });

    this.event  = new Event();
  }

  clearRadioBtns() {
    let radioBtns = this.el.nativeElement.querySelectorAll('#option-create, #option-info, #option-tags');
    for(let i = 0; i < radioBtns.length; i++) {
      radioBtns[i].parentElement.classList.remove('is-checked');
    }
  }

  onLocationSelected(e) {
    this.event.lat = e.lat;
    this.event.lng = e.lng;

    this.wizardStep = 'title-description';
    this.step2 = true;
    this.clearRadioBtns();
    this.el.nativeElement.querySelector('#option-info').parentElement.classList.add('is-checked');
    this.cancelBtnText = 'Back';
    this.showNextBtn = true;
  }

  onBackBtn() {
    if(this.wizardStep === 'create') {
      this.onFinish();
    }
    else if(this.wizardStep === 'title-description') {
      this.clearRadioBtns();
      this.el.nativeElement.querySelector('#option-create').parentElement.classList.add('is-checked');
      this.cancelBtnText = 'Cancel';
      this.wizardStep = 'create';
      this.showNextBtn = false;

      this.event.description = '';
      this.event.title = '';
      this.event.eta = '';
    }
    else if(this.wizardStep == 'tags') {
      this.clearRadioBtns();
      this.el.nativeElement.querySelector('#option-info').parentElement.classList.add('is-checked');
      this.cancelBtnText = 'Back';
      this.wizardStep = 'title-description';
      this.showNextBtn = true;
      this.showSubmitBtn = false;

      this.event.description = '';
      this.event.title = '';
      this.event.eta = '';

    }
  }

  onNextBtn() {
    if(this.wizardStep === 'title-description') {
      this.clearRadioBtns();
      this.el.nativeElement.querySelector('#option-tags').parentElement.classList.add('is-checked');
      this.cancelBtnText = 'Back';
      this.wizardStep = 'tags';
      this.showNextBtn = false;
      this.showSubmitBtn = true;
    }
  }

  onSubmit(form: any): void {
    this.eventService.addEvent(this.event);
    this.wizardStep = 'success';
    this.showSuccessBtn = true;
    this.showSubmitBtn = false;
    this.showCancelBtn = false;
  }

  toggleCategory(category) {
    if (this.event.categories) {
      let index = this.event.categories.indexOf(category);
      if (index == -1) {
        this.event.categories.push(category);
      } else {
        this.event.categories.splice(index, 1);
      }
    } else {
      this.event.categories = [category];
    }
  }

  onFinish() {
    this.router.navigate(['/']);
  }
}
