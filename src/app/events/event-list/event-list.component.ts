import { Component, OnInit } from '@angular/core';
import { AngularFire, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { Event, EventService } from '../shared';
import { EventComponent } from '../event/event.component';
import { MapComponent } from '../../map';
import { SidemenuComponent } from '../../sidemenu';

@Component({
  moduleId: module.id,
  selector: 'app-event-list',
  templateUrl: 'event-list.component.html',
  styleUrls: ['event-list.component.css'],
  directives: [ MapComponent, SidemenuComponent ]
})

export class EventListComponent implements OnInit {
  events: FirebaseListObservable<any[]>;

  constructor(private eventService: EventService) {}

  ngOnInit() {
    this.events = this.eventService.getEvents();
  }
}
