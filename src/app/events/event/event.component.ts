import { Component, Input  } from '@angular/core';
import { RouteSegment } from '@angular/router';

import { Event, EventService } from '../shared';

@Component({
  moduleId: module.id,
  selector: 'cf-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.css']
})
export class EventComponent {
  @Input() event: Event;
}
