import {
  beforeEach,
  beforeEachProviders,
  describe,
  expect,
  it,
  inject,
} from '@angular/core/testing';
import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { EventComponent } from './event.component';
import { Event } from '../shared';

describe('Component: Event', () => {
  let builder: TestComponentBuilder;

  beforeEachProviders(() => [EventComponent]);
  beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
    builder = tcb;
  }));

  it('should inject the component', inject([EventComponent],
      (component: EventComponent) => {
    expect(component).toBeTruthy();
  }));

  it('should create the component', inject([], () => {
    return builder.createAsync(EventComponentTestController)
      .then((fixture: ComponentFixture<any>) => {
        let query = fixture.debugElement.query(By.directive(EventComponent));
        expect(query).toBeTruthy();
        expect(query.componentInstance).toBeTruthy();
      });
  }));

  it('should show title and description of event', inject([], () => {
    return builder.createAsync(EventComponent)
      .then((fixture: ComponentFixture<any>) => {
        let detail = fixture.componentInstance,
            element = fixture.nativeElement,
            event: Event = {
              id: 'test',
              title: 'event1',
              description: 'desc1',
              eta: '',
              createdat: '',
              lat: 1,
              lng: 1,
              categories: []
            };

        detail.event = event;
        fixture.detectChanges();

        expect(element.querySelector('h1').innerText).toBe('event1');
        expect(element.querySelector('p').innerText).toBe('desc1');
      });
  }));
});

@Component({
  selector: 'test',
  template: `
    <app-event></app-event>
  `,
  directives: [EventComponent]
})
class EventComponentTestController {
}

