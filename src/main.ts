import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { CityfuzzAppComponent, environment } from './app/';
import { FIREBASE_PROVIDERS, defaultFirebase } from 'angularfire2';
import { ANGULAR2_GOOGLE_MAPS_PROVIDERS } from 'angular2-google-maps/core'
import { ROUTER_PROVIDERS } from '@angular/router';

if (environment.production) {
  enableProdMode();
}

bootstrap(CityfuzzAppComponent, [
  FIREBASE_PROVIDERS,
  defaultFirebase('https://cityfuzz.firebaseio.com'),
  ANGULAR2_GOOGLE_MAPS_PROVIDERS,
  ROUTER_PROVIDERS
]);
